import axios from 'axios'

const state = {
  dnacIp: 'sandboxdnac2.cisco.com',
  dnacToken: null,
  healthDistribution: [],
  issues: [],
  loading: false
}

const getters = {
  healthDistribution: state => state.healthDistribution,
  issues: state => state.issues,
  loading: state => state.loading
}

const mutations = {
  SET_DNAC_IP(state, newIp) {
    state.dnacIp = newIp
  },
  SET_CISCO_DNA_TOKEN(state, dnacToken) {
    state.dnacToken = dnacToken
  },
  SET_HEALTH_DISTRIBUTION: (state, distribution) => {
    state.healthDistribution = distribution
  },
  SET_ISSUES: (state, issues) => {
    state.issues = issues
  },
  SET_LOADING: (state, loading) => {
    state.loading = loading
  }
}

const actions = {
  async getDnacToken({ commit }) {
    const username = 'devnetuser'
    const password = 'Cisco123!'
    const auth = btoa(`${username}:${password}`)
    const clusterIp = '/dna/system/api/v1/auth/token'

    try {
      const response = await axios({
        url: clusterIp,
        method: 'post',
        headers: { 'Authorization': `Basic ${auth}` }
      })
      const dnacToken = response.data.Token
      commit('SET_CISCO_DNA_TOKEN', dnacToken)
    } catch (error) {
      console.error('API request failed:', error)
      throw error
    }
  },

  fetchNetworkHealth({ commit, state }) {
    const dnacToken = state.dnacToken // Vuex 스토어에서 dnacToken 가져오기

    // axios 호출의 Promise를 명시적으로 반환합니다.
    return axios.get(`/dna/intent/api/v1/network-health`, {
      headers: {
        'X-Auth-Token': dnacToken
      }
    })
      .then(response => {
        commit('SET_HEALTH_DISTRIBUTION', response.data.healthDistirubution)
      })
      .catch(error => {
        console.error('데이터 가져오기 오류:', error)
        throw error // 오류를 다시 throw하여 호출자가 catch할 수 있도록 합니다.
      })
  },

  fetchIssues({ commit, state }) {
    commit('SET_LOADING', true)
    axios.get('/dna/intent/api/v1/issues', {
      headers: {
        'X-Auth-Token': state.dnacToken
      }
    })
      .then(response => {
        commit('SET_ISSUES', response.data.response)
        commit('SET_LOADING', false)
      })
      .catch(error => {
        console.error('이슈 데이터 가져오기 오류:', error)
        commit('SET_LOADING', false)
      })
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
