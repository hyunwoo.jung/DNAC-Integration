import request from '@/utils/request'

export function getDnacToken(data) {
  return request({
    url: '/dna/system/api/v1/auth/token',
    method: 'post',
    data
  })
}

export function fetchNetworkHealth(data) {
  return request({
    url: '/dna/intent/api/v1/network-health',
    method: 'get',
    data
  })
}

export function fetchIssues(data) {
  return request({
    url: '/dna/intent/api/v1/issues',
    method: 'get',
    data
  })
}
